﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CubePieceType {
    CORNER,
    EDGE,
    CENTRE
}

[Serializable]
public class CubePieceProperties {
    public CubeFace face;
    public MeshRenderer faceMesh;
}

public class CubePieceBehaviour : MonoBehaviour {

    [SerializeField] protected CubePieceType type;
    [SerializeField] protected List<CubePieceProperties> faces;

    List<CubeFace> faceList;

    public string[] rotatoryAxisParentNames;

    public CubePieceType CubePieceType {
        get {
            return type;
        }
    }

    public List<CubeFace> Faces {
        get {
            if(faceList == null) {
                faceList = new List<CubeFace>();
                for(int i = 0; i < faces.Count; i++) {
                    faceList.Add(faces[i].face);
                }
            }

            return faceList;
        }
    }

    public string XAxisParent {
        get {
            return rotatoryAxisParentNames[0];
        }
        set {
            rotatoryAxisParentNames[0] = value;
        }
    }

    public string YAxisParent {
        get {
            return rotatoryAxisParentNames[1];
        }
        set {
            rotatoryAxisParentNames[1] = value;
        }
    }

    public string ZAxisParent {
        get {
            return rotatoryAxisParentNames[2];
        }
        set {
            rotatoryAxisParentNames[2] = value;
        }
    }

    public void Init(string xAxisParent, string yAxisParent, string zAxisParent) {
        ApplyFaceProperties();

        rotatoryAxisParentNames = new string[3];
        XAxisParent = xAxisParent;
        YAxisParent = yAxisParent;
        ZAxisParent = zAxisParent;
    }

    public void Init(string xAxisParent, string yAxisParent, string zAxisParent, CubePieceState savedState) {
        LoadFaceProperties(savedState);

        rotatoryAxisParentNames = new string[3];
        XAxisParent = xAxisParent;
        YAxisParent = yAxisParent;
        ZAxisParent = zAxisParent;
    }

    void ApplyFaceProperties() {
        foreach(var face in faces) {
            ApplyFaceDirection(face);
            face.faceMesh.sharedMaterial = CubeManager.Instance.GetFaceProperties(face.face).faceMaterial;

            CubeManager.Instance.SetCubeSolvedState(face.face, face.faceMesh.transform.forward, true);
        }
    }

    void LoadFaceProperties(CubePieceState savedState) {
        transform.rotation = Quaternion.Euler(savedState.xRot, savedState.yRot, savedState.zRot);

        for(int i = 0; i < faces.Count; i++) {
            faces[i].face = savedState.faces[i];
            faces[i].faceMesh.sharedMaterial = CubeManager.Instance.GetFaceProperties(faces[i].face).faceMaterial;

            CubeManager.Instance.SetCubeSolvedState(faces[i].face, faces[i].faceMesh.transform.forward, true);
        }
    }

    public void PrepareForRotation(string axisName) {
        foreach(var face in faces) {
            CubeManager.Instance.SetCubeSolvedState(face.face, face.faceMesh.transform.forward, false);
        }

        for(int i = 0; i < rotatoryAxisParentNames.Length; i++) {
            if(rotatoryAxisParentNames[i] == axisName)
                continue;

            CubeManager.Instance.UnlinkRotatoryParentChild(this, rotatoryAxisParentNames[i]);
            rotatoryAxisParentNames[i] = string.Empty;
        }
    }

    public void OnRotationCompleted() {
        foreach(var face in faces) {
            CubeManager.Instance.SetCubeSolvedState(face.face, face.faceMesh.transform.forward, true);
        }
    }

    void ApplyFaceDirection(CubePieceProperties face) {
        if(face.faceMesh.transform.forward == Vector3.forward) {
            face.face = CubeFace.LEFT;
        } else if(face.faceMesh.transform.forward == Vector3.back) {
            face.face = CubeFace.RIGHT;
        } else if(face.faceMesh.transform.forward == Vector3.up) {
            face.face = CubeFace.BOTTOM;
        } else if(face.faceMesh.transform.forward == Vector3.down) {
            face.face = CubeFace.TOP;
        } else if(face.faceMesh.transform.forward == Vector3.right) {
            face.face = CubeFace.BACK;
        } else if(face.faceMesh.transform.forward == Vector3.left) {
            face.face = CubeFace.FRONT;
        }
    }

}
