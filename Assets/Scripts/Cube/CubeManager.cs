﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CubeFace {
    FRONT,
    BACK,
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}

[Serializable]
public class FaceProperties {
    public CubeFace face;
    public Material faceMaterial;
    public int colorIndex;
}

[Serializable]
public class RotatoryParentInfo {
    public string name;
    public Transform transform;
    public List<CubePieceBehaviour> children;
}

[Serializable]
public class CubePieceState {
    //Getting a serialization error when saving game state using Vector3
    public float xRot, yRot, zRot;
    public List<CubeFace> faces;
}

public class CubeManager : Singleton<CubeManager> {

    [SerializeField] List<Color32> colorOptions;

    [SerializeField] List<FaceProperties> faceProperties;
    [SerializeField] CubePieceBehaviour cornerPiece;
    [SerializeField] CubePieceBehaviour edgePiece;
    [SerializeField] CubePieceBehaviour centrePiece;

    [SerializeField] Transform cubePieceParent;
    [SerializeField] int cubeSize;

    [SerializeField] GameObject rotatoryParentPrefab;
    [SerializeField] Transform xAxisRotatoryParents;
    [SerializeField] Transform yAxisRotatoryParents;
    [SerializeField] Transform zAxisRotatoryParents;

    Dictionary<CubeFace, Dictionary<string, int>> currentSolvedState;
    public bool isCubeSolved;

    Dictionary<string, RotatoryParentInfo> rotatoryParents;
    bool inRotation;

    public bool InRotation {
        get {
            return inRotation;
        }
    }

    public void Awake() {
        currentSolvedState = new Dictionary<CubeFace, Dictionary<string, int>>();
        rotatoryParents = new Dictionary<string, RotatoryParentInfo>();

        inRotation = false;

        //Saving/Loading the colour configuration
        foreach(FaceProperties faceProperty in faceProperties) {
            if(PlayerPrefs.HasKey(faceProperty.face.ToString())) {
                faceProperty.colorIndex = PlayerPrefs.GetInt(faceProperty.face.ToString());
                faceProperty.faceMaterial.color = colorOptions[faceProperty.colorIndex];
            } else {
                PlayerPrefs.SetInt(faceProperty.face.ToString(), faceProperty.colorIndex);
            }
        }
    }

    public void ConstructCube(int cubeSize, bool loadSave) {
        DeleteExistingCube();

        CubePieceState[][][] cubeState = null;
        if(loadSave) {
            cubeState = GameManager.Instance.GameSaveState.cubeState;
        }

        this.cubeSize = cubeSize;
        float startingOffset = (cubeSize - 1) / 2f;

        //Roratory parents are instantiated to map to each pivoting axis of the cube
        //so that we can just rotate the axis in the given direction after temporarily parenting
        //the cube pieces

        float xPos = startingOffset;
        for(int i = 0; i < cubeSize; i++) {
            string xAxisParentName = "x" + i;
            if(!rotatoryParents.ContainsKey(xAxisParentName)) {
                GenerateRotatoryParent(xAxisParentName, xPos, xAxisRotatoryParents);
            }

            float yPos = startingOffset;
            for(int j = 0; j < cubeSize; j++) {
                string yAxisParentName = "y" + j;
                if(!rotatoryParents.ContainsKey(yAxisParentName)) {
                    GenerateRotatoryParent(yAxisParentName, yPos, yAxisRotatoryParents);
                }

                float zPos = startingOffset;
                for(int k = 0; k < cubeSize; k++) {
                    string zAxisParentName = "z" + k;
                    if(!rotatoryParents.ContainsKey(zAxisParentName)) {
                        GenerateRotatoryParent(zAxisParentName, zPos, zAxisRotatoryParents);
                    }

                    CubePieceBehaviour cubePiece = SelectCubePiecePrefab(i, j, k);
                    if(cubePiece) {
                        CubePieceBehaviour newPiece = GameObject.Instantiate(cubePiece, new Vector3(xPos, yPos, zPos), GetRotation(i, j, k, cubePiece.CubePieceType), cubePieceParent) as CubePieceBehaviour;

                        if(loadSave)
                            newPiece.Init(xAxisParentName, yAxisParentName, zAxisParentName, cubeState[i][j][k]);
                        else
                            newPiece.Init(xAxisParentName, yAxisParentName, zAxisParentName);

                        rotatoryParents[xAxisParentName].children.Add(newPiece);
                        rotatoryParents[yAxisParentName].children.Add(newPiece);
                        rotatoryParents[zAxisParentName].children.Add(newPiece);
                    }

                    zPos -= 1f;
                }

                yPos -= 1f;
            }

            xPos -= 1f;
        }
    }

    public CubePieceState[][][] GetCubeState() {
        CubePieceState[][][] cubeState = new CubePieceState[cubeSize][][];

        for(int i = 0; i < cubeState.Length; i++) {
            cubeState[i] = new CubePieceState[cubeSize][];

            for(int j = 0; j < cubeSize; j++) {
                cubeState[i][j] = new CubePieceState[cubeSize];
            }
        }

        foreach(CubePieceBehaviour cubePiece in cubePieceParent.GetComponentsInChildren<CubePieceBehaviour>()) {
            int i = int.Parse(cubePiece.XAxisParent.Substring(1));
            int j = int.Parse(cubePiece.YAxisParent.Substring(1));
            int k = int.Parse(cubePiece.ZAxisParent.Substring(1));

            cubeState[i][j][k] = new CubePieceState {
                faces = new List<CubeFace>(cubePiece.Faces),
                xRot = cubePiece.transform.rotation.eulerAngles.x,
                yRot = cubePiece.transform.rotation.eulerAngles.y,
                zRot = cubePiece.transform.rotation.eulerAngles.z
            };
        }

        return cubeState;
    }

    public void Rotate(string axisName, bool clockwise, float animationTime = 0.5f) {
        StartCoroutine(StartRotation(axisName, clockwise, animationTime));
    }

    public void Rotate(Vector3 dragDirection, Vector3 cubePiecePosition, Vector3 cubeFaceDirection) {
        Vector3 pivotAxis = Vector3.one;
        bool clockwise = true;

        //Eliminating axis
        pivotAxis -= new Vector3(Mathf.Abs(dragDirection.x), Mathf.Abs(dragDirection.y), Mathf.Abs(dragDirection.z));
        pivotAxis -= new Vector3(Mathf.Abs(cubeFaceDirection.x), Mathf.Abs(cubeFaceDirection.y), Mathf.Abs(cubeFaceDirection.z));

        //We only need to pick the axis that has the highest absolute value
        dragDirection = new Vector3(Mathf.Round(dragDirection.x), Mathf.Round(dragDirection.y), Mathf.Round(dragDirection.z));

        string axisName = string.Empty;
        if(pivotAxis.x > pivotAxis.y && pivotAxis.x > pivotAxis.z) {
            axisName = "x" + Mathf.Round((cubeSize - 1) / 2f - cubePiecePosition.x);
            clockwise = dragDirection.z < 0 || dragDirection.y > 0 ? false : true;
        } else if(pivotAxis.y > pivotAxis.x && pivotAxis.y > pivotAxis.z) {
            axisName = "y" + Mathf.Round((cubeSize - 1) / 2f - cubePiecePosition.y);
            clockwise = dragDirection.x < 0 || dragDirection.z > 0 ? false : true;
        } else if(pivotAxis.z > pivotAxis.y && pivotAxis.z > pivotAxis.x) {
            axisName = "z" + Mathf.Round((cubeSize - 1) / 2f - cubePiecePosition.z);
            clockwise = dragDirection.y < 0 || dragDirection.x > 0 ? false : true;
        }

        //Cube faces that are on the opposite side need a reversal of pivot direction
        if((cubeFaceDirection.x + cubeFaceDirection.y + cubeFaceDirection.z) > 0) {
            clockwise = !clockwise;
        }

        if(!inRotation) {
            StartCoroutine(StartRotation(axisName, clockwise));
            GameManager.Instance.AddToUndoStack(axisName, clockwise);
        }
    }

    void DeleteExistingCube() {
        if(currentSolvedState == null)
            return;

        foreach(Transform parent in transform) {
            foreach(Transform child in parent) {
                GameObject.Destroy(child.gameObject);
            }
        }

        currentSolvedState.Clear();
        rotatoryParents.Clear();
    }

    void GenerateRotatoryParent(string name, float pos, Transform parent) {
        RotatoryParentInfo rotatoryParentInfo = new RotatoryParentInfo();
        rotatoryParentInfo.name = name;
        rotatoryParentInfo.children = new List<CubePieceBehaviour>();

        rotatoryParentInfo.transform = GameObject.Instantiate(rotatoryParentPrefab, parent).transform;
        rotatoryParentInfo.transform.localPosition = new Vector3(pos, 0, 0);
        rotatoryParentInfo.transform.localRotation = Quaternion.identity;
        rotatoryParentInfo.transform.name = name;

        rotatoryParents.Add(name, rotatoryParentInfo);
    }

    CubePieceBehaviour SelectCubePiecePrefab(int i, int j, int k) {
        int faceCount = 0;

        if(i == 0 || i == cubeSize - 1)
            faceCount++;
        if(j == 0 || j == cubeSize - 1)
            faceCount++;
        if(k == 0 || k == cubeSize - 1)
            faceCount++;

        switch(faceCount) {
            case 3:
                return cornerPiece;
            case 2:
                return edgePiece;
            case 1:
                return centrePiece;
            default:
                return null;
        }
    }

    //TODO : Think of cleaner solutions to calculate rotation
    Quaternion GetRotation(int i, int j, int k, CubePieceType type) {
        float xRot = 0f;
        float yRot = 0f;
        float zRot = 0f;

        switch(type) {
            case CubePieceType.CORNER:
                if(i == cubeSize - 1) {
                    yRot += 270f;
                }
                if(j == cubeSize - 1) {
                    xRot += 180f;
                    yRot -= 90f;
                }
                if(k == cubeSize - 1) {
                    yRot += i == cubeSize - 1 ? -90 : 90f;
                }
                break;
            case CubePieceType.EDGE:
                if(i == 0) {
                    if(j > 0)
                        xRot += 90f;
                    if(j == cubeSize - 1)
                        xRot += 90f;
                    if(k == cubeSize - 1)
                        xRot += 180f;
                }
                else if(i == cubeSize - 1) {
                    if(j == 0)
                        zRot = 90f;
                    else if(j < cubeSize - 1)
                        xRot = 90f;
                    else
                        zRot = 180f;

                    if(k == 0)
                        yRot = -90f;
                    else if(k == cubeSize - 1)
                        yRot = -180f;
                }
                else {
                    if(k == 0)
                        yRot -= 90f;
                    else
                        yRot += 90f;

                    if(j == cubeSize - 1)
                        zRot -= 90f;
                }
                break;
            case CubePieceType.CENTRE:
                if(j == cubeSize - 1)
                    zRot = 180f;
                else if(i == 0)
                    zRot = -90f;
                else if(i == cubeSize - 1)
                    zRot = 90f;
                else if(k == 0)
                    xRot = 90f;
                else if(k == cubeSize - 1)
                    xRot = -90f;
                break;
        }

        return Quaternion.Euler(xRot, yRot, zRot);
    }
    
    IEnumerator StartRotation(string axisName, bool clockwise, float animationTime = 0.5f) {
        inRotation = true;

        foreach(CubePieceBehaviour child in rotatoryParents[axisName].children) {
            child.transform.parent = rotatoryParents[axisName].transform;
            child.PrepareForRotation(axisName);
        }
        
        iTween.RotateBy(rotatoryParents[axisName].transform.gameObject, iTween.Hash("amount", Vector3.right * 90f * (clockwise ? 1f : -1f) / 360f,
            "time", animationTime, "easetype", iTween.EaseType.easeOutQuint));

        yield return new WaitForSeconds(animationTime);

        isCubeSolved = true;
        foreach(CubePieceBehaviour child in rotatoryParents[axisName].children) {
            child.transform.parent = cubePieceParent;
            child.OnRotationCompleted();
            LinkRotatoryParentChild(child);
        }

        if(isCubeSolved) {
            GameManager.Instance.EndGame();
        }
        
        inRotation = false;
    }

    public void UnlinkRotatoryParentChild(CubePieceBehaviour cubePiece, string axisName) {
        rotatoryParents[axisName].children.Remove(cubePiece);
    }

    public void LinkRotatoryParentChild(CubePieceBehaviour cubePiece) {
        string axisName = string.Empty;

        if(string.IsNullOrEmpty(cubePiece.XAxisParent)) {
            axisName = "x" + Mathf.Round((cubeSize - 1) / 2f - cubePiece.transform.position.x);

            cubePiece.XAxisParent = axisName;
            rotatoryParents[axisName].children.Add(cubePiece);
        }

        if(string.IsNullOrEmpty(cubePiece.YAxisParent)) {
            axisName = "y" + Mathf.Round((cubeSize - 1) / 2f - cubePiece.transform.position.y);

            cubePiece.YAxisParent = axisName;
            rotatoryParents[axisName].children.Add(cubePiece);
        }

        if(string.IsNullOrEmpty(cubePiece.ZAxisParent)) {
            axisName = "z" + Mathf.Round((cubeSize - 1) / 2f - cubePiece.transform.position.z);

            cubePiece.ZAxisParent = axisName;
            rotatoryParents[axisName].children.Add(cubePiece);
        }
    }

    public void SetCubeSolvedState(CubeFace face, Vector3 faceDirection, bool newState) {
        string direction = faceDirection.ToString();
        int updateValue = newState ? 1 : -1;

        if(!currentSolvedState.ContainsKey(face)) {
            currentSolvedState.Add(face, new Dictionary<string, int>());
        }

        if(!currentSolvedState[face].ContainsKey(direction)) {
            currentSolvedState[face].Add(direction, updateValue);
        } else {
            currentSolvedState[face][direction] += updateValue;
        }

        if(currentSolvedState[face][direction] <= 0) {
            currentSolvedState[face].Remove(direction);
        }

        if(currentSolvedState[face].Keys.Count > 1) {
            isCubeSolved = false;
        }
    }

    public FaceProperties GetFaceProperties(CubeFace face) {
        return faceProperties.Find(f => f.face == face);
    }

    public void RotateCube(Vector3 rotation) {
        iTween.RotateTo(gameObject, iTween.Hash("rotation", rotation, "time", 0.35f, "easetype", iTween.EaseType.spring));
    }

    public void UpdateFaceColor(CubeFace face, bool nextColor) {
        switch(face) {
            case CubeFace.TOP:
            case CubeFace.FRONT:
            case CubeFace.RIGHT:
                if(transform.rotation.eulerAngles != Vector3.zero) {
                    RotateCube(Vector3.zero);
                }
                break;
            default:
                if(transform.rotation.eulerAngles != new Vector3(0f, -90f, -180f)) {
                    RotateCube(new Vector3(0f, -90f, -180f));
                }
                break;
        }

        var faceProperty = faceProperties.Find(fp => fp.face == face);

        do {
            faceProperty.colorIndex += colorOptions.Count + (nextColor ? 1 : -1);
            faceProperty.colorIndex %= colorOptions.Count;
        } while(faceProperties.Exists(fp => fp != faceProperty && fp.colorIndex == faceProperty.colorIndex));

        PlayerPrefs.SetInt(faceProperty.face.ToString(), faceProperty.colorIndex);
        faceProperty.faceMaterial.color = colorOptions[faceProperty.colorIndex];
    }
}
