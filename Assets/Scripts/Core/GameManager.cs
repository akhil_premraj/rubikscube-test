﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Undo {
    public string axisName;
    public bool clockwise;
}

[Serializable]
public class GameSaveState {
    public int cubeSize;
    public Undo[] undoStack;
    public CubePieceState[][][] cubeState;
    public int time;
}

public class GameManager : Singleton<GameManager> {

    [SerializeField] int cubeSize;

    [SerializeField] Transform raycastPlane;
    [SerializeField] LayerMask raycastLayer;
    
    [SerializeField] float zoomSpeed;
    [SerializeField] float dragSpeed;
    [SerializeField] float rotateSpeed;
    [SerializeField] float drageThreshold;

    Vector3 cameraStartingPoint;

    Stack<Undo> undoStack;

    const int minCubeSize = 2;
    const int maxCubeSize = 6;

    float cameraDistance;
    float minCameraDistance;
    float maxCameraDistance;

    CubePieceBehaviour selectedPiece;
    Transform selectedFace;
    Vector3 lastHitPoint;

    bool gameEnded;
    bool gameStarted;
    bool gamePaused;
    bool timeStarted;

    GameSaveState saveState;

    public int MinCubeSize {
        get {
            return minCubeSize;
        }
    }

    public int MaxCubeSize {
        get {
            return maxCubeSize;
        }
    }

    public int CubeSize {
        get {
            return cubeSize;
        }
        set {
            cubeSize = value;
        }
    }

    public GameSaveState GameSaveState {
        get {
            return saveState;
        }
    }

    public bool InMainMenu { get; set; }

    public void Start() {
        cameraStartingPoint = Camera.main.transform.position;
        raycastPlane.gameObject.SetActive(false);
        
        if(File.Exists(Application.persistentDataPath + "/save_state")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(Application.persistentDataPath + "/save_state");

            saveState = (GameSaveState)bf.Deserialize(file);
            file.Close();
        }

        CanvasManager.Instance.EnableContinueButton(saveState != null);

        Reset();

        StartCoroutine(IdleAnimationHandler());
    }

    public void Reset() {
        Camera.main.transform.position = cameraStartingPoint;
        Camera.main.transform.LookAt(Vector3.zero);

        undoStack = new Stack<Undo>();
        if(saveState != null) {
            for(int i = saveState.undoStack.Length - 1; i >= 0; i--) {
                undoStack.Push(saveState.undoStack[i]);
            }

            cubeSize = saveState.cubeSize;
            ConstructCube();
        } else {
            cubeSize = 3;
            ConstructCube();
        }

        gameEnded = false;
        gamePaused = false;
        gameStarted = false;
        timeStarted = false;
        idleAnimation = false;
    }

    public void SaveStateAndExit() {
        saveState = new GameSaveState();

        saveState.cubeSize = cubeSize;
        saveState.undoStack = undoStack.ToArray();
        saveState.cubeState = CubeManager.Instance.GetCubeState();
        saveState.time = CanvasManager.Instance.Time;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/save_state");
        bf.Serialize(file, saveState);
        file.Close();

        Reset();
    }

    public void UnloadSaveState() {
        saveState = null;
        Reset();
    }

    public void DeleteSaveData() {
        saveState = null;
        if(File.Exists(Application.persistentDataPath + "/save_state")) {
            File.Delete(Application.persistentDataPath + "/save_state");
        }

        CanvasManager.Instance.EnableContinueButton(false);
    }

    public void Update() {
        if(gameStarted && Input.GetKeyUp(KeyCode.Escape)) {
            if(!gamePaused)
                CanvasManager.Instance.OnExitClicked();
            else
                CanvasManager.Instance.OnExitCancelled();
        }

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, -Camera.main.transform.forward.normalized * cameraDistance, Time.deltaTime * zoomSpeed);

        if(gameStarted && !gamePaused) {
            if(Input.GetMouseButtonDown(0)) {
                RaycastHit raycastHit;

                if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit, raycastLayer)) {
                    selectedPiece = raycastHit.collider.GetComponentInParent<CubePieceBehaviour>();
                    selectedFace = raycastHit.collider.transform;
                    lastHitPoint = raycastHit.point;

                    raycastPlane.transform.position = raycastHit.collider.transform.position;
                    raycastPlane.transform.rotation = raycastHit.collider.transform.rotation;
                    raycastPlane.gameObject.SetActive(true);
                } else {
                    selectedPiece = null;
                }
            } else if(Input.GetMouseButton(0)) {
                if(!selectedPiece) {
                    Camera.main.transform.RotateAround(Vector3.zero, Vector3.up, Input.GetAxis("Mouse X") * dragSpeed);

                    //Trying to clamp between 70 and -70 as rotation on the XZ plane is weird.
                    //Camera.main.transform.rotation.eulerAngles sometimes returns 290 instead -70.
                    float xRot = Camera.main.transform.rotation.eulerAngles.x;
                    xRot = xRot > 180 ? xRot - 360f : xRot;
                    if((Input.GetAxis("Mouse Y") < 0 && xRot < 70) || (Input.GetAxis("Mouse Y") > 0 && xRot > -70))
                        Camera.main.transform.position -= Camera.main.transform.up * Input.GetAxis("Mouse Y") * dragSpeed * 0.5f;

                    Camera.main.transform.LookAt(Vector3.zero);
                }
            } else if(Input.GetMouseButtonUp(0)) {
                RaycastHit raycastHit;

                if(selectedPiece && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit, raycastLayer)) {
                    if(Vector3.Distance(lastHitPoint, raycastHit.point) > drageThreshold) {
                        CubeManager.Instance.Rotate((raycastHit.point - lastHitPoint).normalized, selectedPiece.transform.position, selectedFace.forward);

                        if(!timeStarted) {
                            timeStarted = true;
                            CanvasManager.Instance.StartGameTime();
                        }
                    }
                }

                selectedPiece = null;
                raycastPlane.gameObject.SetActive(false);
            }

            cameraDistance -= Input.mouseScrollDelta.y;
            cameraDistance = Mathf.Clamp(cameraDistance, minCameraDistance, maxCameraDistance);
        } else if(gameEnded || idleAnimation) {
            Camera.main.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * rotateSpeed * (idleAnimation ? 0.5f : 1f));

            Vector3 pos = Camera.main.transform.position;
            pos.y = cameraStartingPoint.y;
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, pos, Time.deltaTime * dragSpeed);

            Camera.main.transform.LookAt(Vector3.zero);
        }

        if(Input.GetMouseButtonDown(0)) {
            lastMouseClickTime = Time.time;

            if(idleAnimation) {
                Reset();
            }
        }
    }

    public void StartNewGame() {
        DeleteSaveData();
        StartCoroutine(Scramble());
        CanvasManager.Instance.ResetTime(0);
    }

    public void ContinueGame() {
        gameStarted = true;
        CanvasManager.Instance.ResetTime(saveState.time);
    }

    public void PauseGame(bool pause) {
        gamePaused = pause;
    }

    public void EndGame() {
        if(!gameStarted)
            return;

        gameEnded = true;
        gameStarted = false;

        DeleteSaveData();
        CanvasManager.Instance.OnEndGame();
    }

    float ClampAngle(float angle, float min, float max) {
        if(angle < -360F)
            angle += 360F;
        if(angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
    
    public void ConstructCube() {
        minCameraDistance = cubeSize * 2;
        maxCameraDistance = minCameraDistance + 3;
        cameraDistance = (minCameraDistance + maxCameraDistance) / 2f;

        CubeManager.Instance.ConstructCube(cubeSize, saveState != null);
    }

    public void AddToUndoStack(string axisName, bool clockwise) {
        undoStack.Push(new Undo {
            axisName = axisName,
            clockwise = clockwise
        });
    }

    public void Undo() {
        if(CubeManager.Instance.InRotation || undoStack.Count == 0)
            return;

        Undo undoMove = undoStack.Pop();
        CubeManager.Instance.Rotate(undoMove.axisName, !undoMove.clockwise);
    }

    IEnumerator Scramble() {
        int moveCount = UnityEngine.Random.Range(10, 20);

        for(int i = 0; i < moveCount; i++) {
            RotateAxis(i, 0.25f);

            yield return new WaitForSeconds(0.25f);
            yield return null;
        }

        gameStarted = true;
    }

    float lastMouseClickTime;
    bool idleAnimation;
    IEnumerator IdleAnimationHandler() {
        while(true) {
            if(InMainMenu && (Time.time - lastMouseClickTime) > 10f) {
                if(!idleAnimation) {
                    idleAnimation = true;
                    CubeManager.Instance.ConstructCube(CubeSize, false);
                }

                RotateAxis(UnityEngine.Random.Range(0, 100), 1f);

                yield return new WaitForSeconds(1f);
                yield return null;
            }

            yield return null;
        }
    }

    void RotateAxis(int randomNumer, float time) {
        string axisName = string.Empty;
        switch(randomNumer % 3) {
            case 0:
                axisName = "x";
                break;
            case 1:
                axisName = "y";
                break;
            case 2:
                axisName = "z";
                break;
        }

        axisName += UnityEngine.Random.Range(0, cubeSize);

        CubeManager.Instance.Rotate(axisName, Mathf.Sign(UnityEngine.Random.Range(-1f, 1f)) > 0, time);
    }
}
