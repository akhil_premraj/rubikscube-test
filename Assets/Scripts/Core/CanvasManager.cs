﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : Singleton<CanvasManager> {

    [SerializeField] CanvasGroup mainMenu;
    [SerializeField] CanvasGroup newGameOptions;
    [SerializeField] CanvasGroup customizeOptions;
    [SerializeField] CanvasGroup gameHUD;
    [SerializeField] CanvasGroup exitConfirmation;
    [SerializeField] CanvasGroup winScreen;

    [SerializeField] AnimationClip showMainMenuClip;
    [SerializeField] AnimationClip closeMainMenuClip;
    [SerializeField] AnimationClip showNewGameOptionsClip;
    [SerializeField] AnimationClip closeNewGameOptionsClip;
    [SerializeField] AnimationClip showCustomizeOptionsClip;
    [SerializeField] AnimationClip closeCustomizeOptionsClip;
    [SerializeField] AnimationClip showGameHUDClip;
    [SerializeField] AnimationClip closeGameHUDClip;
    [SerializeField] AnimationClip showExitConfirmationClip;
    [SerializeField] AnimationClip closeExitConfirmationClip;
    [SerializeField] AnimationClip showWinScreenClip;
    [SerializeField] AnimationClip closeWinScreenClip;

    [SerializeField] Button continueButton;
    [SerializeField] Image stopwatchBg;

    [SerializeField] Text cubeSizeText;
    [SerializeField] Text stopwatch;
    [SerializeField] Text endGameTime;

    public iTween.EaseType easetypeHelper;

    const float fadeTime = 0.5f;

    int time;
    int hours;
    int minutes;
    int seconds;
    bool pauseTime;

    Animation animation;

    public int Time {
        get {
            return time;
        }
    }

    public void Start() {
        animation = GetComponent<Animation>();

        newGameOptions.gameObject.SetActive(false);
        newGameOptions.alpha = 0f;

        customizeOptions.gameObject.SetActive(false);
        customizeOptions.alpha = 0f;

        gameHUD.gameObject.SetActive(false);
        gameHUD.alpha = 0f;

        exitConfirmation.gameObject.SetActive(false);
        exitConfirmation.alpha = 0f;

        winScreen.gameObject.SetActive(false);
        winScreen.alpha = 0f;

        ShowMainMenuScreen();
        GameManager.Instance.InMainMenu = true;

        pauseTime = false;

        stopwatchBg.color = Color.grey;
    }

    public void EnableContinueButton(bool enable) {
        continueButton.interactable = enable;
    }

    IEnumerator DelayAnimation(Action animation, float time) {
        yield return new WaitForSeconds(time);
        animation();
    }

    public void OnNewGameClicked() {
        EnableContinueButton(false);
        GameManager.Instance.UnloadSaveState();
        cubeSizeText.text = GameManager.Instance.CubeSize.ToString();

        ShowNewGameOptions();

        GameManager.Instance.InMainMenu = false;
    }

    public void OnStartGameClicked() {
        CloseNewGameOptions();
        StartCoroutine(DelayAnimation(CloseMainMenuScreen, 1f));
        StartCoroutine(DelayAnimation(ShowGameHUDScreen, 2f));
        
        GameManager.Instance.StartNewGame();

        GameManager.Instance.InMainMenu = false;
    }

    public void OnContinueClicked() {
        if(newGameOptions.alpha > 0.5f) {
            CloseNewGameOptions();
            StartCoroutine(DelayAnimation(CloseMainMenuScreen, 1f));
            StartCoroutine(DelayAnimation(ShowGameHUDScreen, 2f));
        } else {
            CloseMainMenuScreen();
            StartCoroutine(DelayAnimation(ShowGameHUDScreen, 1f));
        }

        GameManager.Instance.ContinueGame();

        GameManager.Instance.InMainMenu = false;
    }

    public void OnEndGame() {
        endGameTime.text = string.Format("Your Time : {0:00}:{1:00}:{2:00}", hours, minutes, seconds);

        CloseGameHUDScreen();
        StartCoroutine(DelayAnimation(ShowWinScreen, 1f));
    }

    public void OnWinScreenBackclicked() {
        ClosewinScreen();
        StartCoroutine(DelayAnimation(ShowMainMenuScreen, 1f));

        GameManager.Instance.Reset();

        GameManager.Instance.InMainMenu = true;

        StopGameTime();
    }

    public void OnExitClicked() {
        ShowExitConfirmationPanel();

        pauseTime = true;
        GameManager.Instance.PauseGame(pauseTime);
    }

    public void OnExitCancelled() {
        CloseExitConfirmationPanel();

        pauseTime = false;
        GameManager.Instance.PauseGame(pauseTime);
    }

    public void OnExitConfirmed() {
        pauseTime = false;
        EnableContinueButton(true);
        
        CloseExitConfirmationPanel();
        StartCoroutine(DelayAnimation(CloseGameHUDScreen, 1f));
        StartCoroutine(DelayAnimation(ShowMainMenuScreen, 2f));

        GameManager.Instance.SaveStateAndExit();
        StopGameTime();

        GameManager.Instance.InMainMenu = true;
    }

    public void OnCustomizeClicked() {
        if(newGameOptions.alpha > 0.5f) {
            CloseNewGameOptions();
            StartCoroutine(DelayAnimation(CloseMainMenuScreen, 1f));
            StartCoroutine(DelayAnimation(ShowCustomizeOptions, 2f));
        } else {
            CloseMainMenuScreen();
            StartCoroutine(DelayAnimation(ShowCustomizeOptions, 1f));
        }

        GameManager.Instance.InMainMenu = false;
    }

    public void OnCloseCustomizeOptions() {
        CloseCustomizeOptions();
        StartCoroutine(DelayAnimation(ShowMainMenuScreen, 1f));

        CubeManager.Instance.RotateCube(Vector3.zero);

        GameManager.Instance.InMainMenu = true;
    }

    public void NextColor(int faceIndex) {
        CubeManager.Instance.UpdateFaceColor((CubeFace)faceIndex, true);
    }

    public void PreviousColor(int faceIndex) {
        CubeManager.Instance.UpdateFaceColor((CubeFace)faceIndex, false);
    }

    void ShowMainMenuScreen() {
        animation.Play(showMainMenuClip.name);
    }

    void CloseMainMenuScreen() {
        animation.Play(closeMainMenuClip.name);
    }

    void ShowNewGameOptions() {
        animation.Play(showNewGameOptionsClip.name);
    }

    void CloseNewGameOptions() {
        animation.Play(closeNewGameOptionsClip.name);
    }

    void ShowCustomizeOptions() {
        animation.Play(showCustomizeOptionsClip.name);
    }

    void CloseCustomizeOptions() {
        animation.Play(closeCustomizeOptionsClip.name);
    }

    void ShowGameHUDScreen() {
        animation.Play(showGameHUDClip.name);
    }

    void CloseGameHUDScreen() {
        animation.Play(closeGameHUDClip.name);
    }

    void ShowExitConfirmationPanel() {
        animation.Play(showExitConfirmationClip.name);
    }

    void CloseExitConfirmationPanel() {
        animation.Play(closeExitConfirmationClip.name);
    }

    void ShowWinScreen() {
        animation.Play(showWinScreenClip.name);
    }

    void ClosewinScreen() {
        animation.Play(closeWinScreenClip.name);
    }

    public void UpdateCubeSize(int value) {
        int cubeSize = GameManager.Instance.CubeSize + value;
        cubeSize = Mathf.Clamp(cubeSize, GameManager.Instance.MinCubeSize, GameManager.Instance.MaxCubeSize);

        cubeSizeText.text = cubeSize.ToString();

        GameManager.Instance.CubeSize = cubeSize;
        GameManager.Instance.ConstructCube();
    }

    public void ResetTime(int previousTime) {
        time = previousTime;

        hours = time / (60 * 60);
        seconds = (time - hours * (60 * 60));
        minutes = seconds / 60;
        seconds = (seconds - minutes * 60);

        stopwatch.text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
    }

    IEnumerator gameTimeCoroutine;

    public void StartGameTime() {
        gameTimeCoroutine = UpdateGameTime();
        StartCoroutine(gameTimeCoroutine);
    }

    public void StopGameTime () {
        if(gameTimeCoroutine != null) {
            StopCoroutine(gameTimeCoroutine);

            stopwatchBg.color = Color.grey;
        }
    }

    IEnumerator UpdateGameTime() {
        stopwatchBg.color = Color.white;

        float t = 0;
        while(true) {
            while(t < 1f) {
                while(pauseTime)
                    yield return null;

                t += UnityEngine.Time.deltaTime;
                yield return null;
            }

            t -= 1f;
            time++;
            seconds++;

            if(seconds >= 60) {
                minutes++;
                seconds -= 60;
            }

            if(minutes >= 60) {
                hours++;
                minutes -= 60;
            }

            stopwatch.text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
        }
    }

}
